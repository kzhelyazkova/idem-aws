import copy
import time

import pytest

EVENT_DESTINATION_NAME = f"idem-test-sns-event-dest-{int(time.time())}"


@pytest.fixture
async def test_ctx(ctx):
    t_ctx = copy.deepcopy(ctx)
    t_ctx["test"] = True
    yield t_ctx


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support sesv2 resource")
async def test_event_destination(hub, ctx, sns_topic, ses_configuration_set):
    # Create a new sns event destination
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert ret["changes"]["new"] == {
        "configuration_set_name": ses_configuration_set,
        "name": EVENT_DESTINATION_NAME,
        "resource_id": EVENT_DESTINATION_NAME,
        "event_destination": {
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    }
    assert not ret["old_state"]

    # Re-apply the same state, no changes expected.
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert not ret["changes"]
    assert ret["result"]
    assert ret["comment"] == (
        f"aws.sesv2.event_destination {EVENT_DESTINATION_NAME!r} already exists",
    )

    # Flip the enabled flag. Ensure only Enabled flag is changed
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": False,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert ret["changes"]["new"] == {"event_destination": {"Enabled": False}}
    assert ret["old_state"]["event_destination"]["Enabled"] is True

    # Delete event destination. Should delete the resource
    ret = await hub.states.aws.sesv2.event_destination.absent(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        resource_id=None,
    )
    assert ret["result"]
    assert ret["changes"]["old"] == {
        "configuration_set_name": ses_configuration_set,
        "name": EVENT_DESTINATION_NAME,
        "resource_id": EVENT_DESTINATION_NAME,
        "event_destination": {
            "Enabled": False,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    }
    assert not ret["new_state"]

    # Delete event destination again, should say resource is already absent
    ret = await hub.states.aws.sesv2.event_destination.absent(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        resource_id=EVENT_DESTINATION_NAME,
    )
    assert ret["result"]
    assert ret["comment"] == (
        f"aws.sesv2.event_destination {EVENT_DESTINATION_NAME!r} already absent",
    )
    assert not ret["changes"]
    assert not ret["old_state"]
    assert not ret["new_state"]


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support sesv2 resource")
async def test_event_destination_dry_run(
    hub, ctx, sns_topic, ses_configuration_set, test_ctx
):
    # Create a new sns event destination
    ret = await hub.states.aws.sesv2.event_destination.present(
        test_ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert ret["changes"]["new"] == {
        "configuration_set_name": ses_configuration_set,
        "name": EVENT_DESTINATION_NAME,
        "resource_id": EVENT_DESTINATION_NAME,
        "event_destination": {
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    }
    assert not ret["old_state"]
    assert ret["comment"] == (
        f"Would create aws.sesv2.event_destination {EVENT_DESTINATION_NAME!r}",
    )

    # Now create the event destination and then re-apply the state in test mode. Should say the resource is
    # already in the correct state.
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert ret["new_state"]
    assert ret["comment"] == (
        f"Created aws.sesv2.event_destination {EVENT_DESTINATION_NAME!r}",
    )
    ret = await hub.states.aws.sesv2.event_destination.present(
        test_ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert not ret["changes"]
    assert ret["new_state"] == ret["old_state"]
    assert ret["comment"] == (
        f"aws.sesv2.event_destination {EVENT_DESTINATION_NAME!r} already exists",
    )

    # Flip the enabled flag. Should say resource will be updated, and changes should only be for Enabled flag.
    ret = await hub.states.aws.sesv2.event_destination.present(
        test_ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": False,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert ret["changes"]["new"] == {"event_destination": {"Enabled": False}}
    assert ret["old_state"]["event_destination"]["Enabled"] is True

    # Delete event destination
    ret = await hub.states.aws.sesv2.event_destination.absent(
        test_ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        resource_id=None,
    )
    assert ret["comment"] == (
        f"Would delete aws.sesv2.event_destination {EVENT_DESTINATION_NAME!r}",
    )
    assert ret["result"]
    assert ret["changes"]["old"]
    assert not ret["new_state"]


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support sesv2 resource")
async def test_event_destination_describe(hub, ctx, sns_topic, ses_configuration_set):
    # Describe before creating a configuration event destination. Should not return results for the current configset
    ret = await hub.states.aws.sesv2.event_destination.describe(ctx)
    assert ses_configuration_set not in ret

    # Create a new sns event destination
    destination = {
        "Enabled": True,
        "MatchingEventTypes": ["BOUNCE"],
        "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
    }
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination=destination,
    )
    assert ret["changes"]["new"]
    assert not ret["old_state"]

    # Describe after creating a configuration event destination. Should return results matching present signature.
    ret = await hub.states.aws.sesv2.event_destination.describe(ctx)
    desc = ret[ses_configuration_set]["aws.sesv2.event_destination.present"]
    assert {"configuration_set_name": ses_configuration_set} in desc
    assert {"resource_id": EVENT_DESTINATION_NAME} in desc
    assert {"name": EVENT_DESTINATION_NAME} in desc
    assert {"event_destination": destination} in desc


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support sesv2 resource")
async def test_configuration_set_event_errors(
    hub, ctx, sns_topic, ses_configuration_set
):
    # Create a new sns event destination with an unknown configuration set, should return error
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name="unknown-config-set",
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert ret["result"] is False
    assert ret["comment"]

    # Create a new sns event destination with an unknown sns topic, should return error
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": "arn:unknown"},
        },
    )
    assert ret["result"] is False
    assert ret["comment"] == (
        "BadRequestException: An error occurred (BadRequestException) when calling the CreateConfigurationSetEventDestination operation: Invalid SNS topic ARN <arn:unknown> should have the form <arn:aws:sns:region:[account-id]:[topic-name]>",
    )

    # Create a new sns event destination with a destination other than SnsDestination, should return error
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "PinpointDestination": {"key": "value"},
        },
    )
    assert ret["comment"] == ("Only SnsDestination is supported now.",)
    assert ret["result"] is False

    # Delete an event destination with an unknown configuration set, should return error
    ret = await hub.states.aws.sesv2.event_destination.absent(
        ctx,
        configuration_set_name="unknown-config-set",
        name=EVENT_DESTINATION_NAME,
        resource_id=None,
    )
    assert ret["result"] is False
    assert ret["comment"]
