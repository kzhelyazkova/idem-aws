import copy
import json
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_subscription(hub, ctx, aws_sns_topic):
    subscription_name = "idem-test-subscription-" + str(uuid.uuid4())
    protocol = "sms"
    endpoint = "+1234"
    topic_arn = aws_sns_topic.get("resource_id")
    return_subscription_arn = True
    delivery_policy = json.dumps(
        {
            "healthyRetryPolicy": {
                "minDelayTarget": 10,
                "maxDelayTarget": 30,
                "numRetries": 10,
                "numMaxDelayRetries": 7,
                "numNoDelayRetries": 0,
                "numMinDelayRetries": 3,
                "backoffFunction": "linear",
            },
            "sicklyRetryPolicy": None,
            "throttlePolicy": None,
            "guaranteed": False,
        },
        separators=(",", ":"),
    )
    attributes = {"DeliveryPolicy": delivery_policy}

    # Create Subscription for a topic with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.sns.subscription.present(
        test_ctx,
        name=subscription_name,
        topic_arn=topic_arn,
        protocol=protocol,
        endpoint=endpoint,
        attributes=attributes,
        return_subscription_arn=return_subscription_arn,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.sns.subscription '{subscription_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert subscription_name == resource.get("name")
    assert topic_arn == resource.get("topic_arn")
    assert protocol == resource.get("protocol")
    assert endpoint == resource.get("endpoint")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)
    assert return_subscription_arn == resource.get("return_subscription_arn")

    # Create Subscription for a topic
    ret = await hub.states.aws.sns.subscription.present(
        ctx,
        name=subscription_name,
        topic_arn=topic_arn,
        protocol=protocol,
        endpoint=endpoint,
        attributes=attributes,
        return_subscription_arn=return_subscription_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("name")
    subscription_name = resource.get("name")
    assert topic_arn == resource.get("topic_arn")
    assert protocol == resource.get("protocol")
    assert endpoint == resource.get("endpoint")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)
    resource_id = resource.get("resource_id")

    # Describe subscriptions
    describe_ret = await hub.states.aws.sns.subscription.describe(ctx)
    assert resource_id in describe_ret
    # Verify that describe output format is correct
    assert "aws.sns.subscription.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.sns.subscription.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert topic_arn == described_resource_map.get("topic_arn")
    assert protocol == described_resource_map.get("protocol")
    assert endpoint == described_resource_map.get("endpoint")
    assert described_resource_map.get("attributes")
    resource_attributes = described_resource_map.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)

    # Updating attributes with test flag
    delivery_policy = json.dumps(
        {
            "healthyRetryPolicy": {
                "minDelayTarget": 12,
                "maxDelayTarget": 20,
                "numRetries": 5,
                "numMaxDelayRetries": 3,
                "numNoDelayRetries": 0,
                "numMinDelayRetries": 2,
                "backoffFunction": "linear",
            },
            "sicklyRetryPolicy": None,
            "throttlePolicy": None,
            "guaranteed": False,
        },
        separators=(",", ":"),
    )
    attributes = {"DeliveryPolicy": delivery_policy}

    ret = await hub.states.aws.sns.subscription.present(
        test_ctx,
        name=subscription_name,
        topic_arn=topic_arn,
        protocol=protocol,
        endpoint=endpoint,
        resource_id=resource_id,
        attributes=attributes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)

    # Updating attributes
    ret = await hub.states.aws.sns.subscription.present(
        ctx,
        name=subscription_name,
        topic_arn=topic_arn,
        protocol=protocol,
        endpoint=endpoint,
        resource_id=resource_id,
        attributes=attributes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_attributes = resource.get("attributes")
    for key, value in attributes.items():
        assert key in resource_attributes
        assert value == resource_attributes.get(key)

    # Delete Subscription with test flags
    ret = await hub.states.aws.sns.subscription.absent(
        test_ctx, name=subscription_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.sns.subscription '{subscription_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete Subscription
    ret = await hub.states.aws.sns.subscription.absent(
        ctx, name=subscription_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.sns.subscription '{subscription_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.sns.subscription.absent(
        ctx, name=subscription_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.sns.subscription '{subscription_name}' already absent" in ret["comment"]
    )
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
