from unittest import mock

import dict_tools
import pytest
from dict_tools.data import NamespaceDict


@pytest.mark.asyncio
async def test_defined_profile_no_region_fallbacks(hub):
    """
    Verify that when region_name is not specified in the acct_file gets populated from ~/.aws/config
    """
    hub.OPT = dict_tools.data.NamespaceDict(idem=dict(awscli_fallbacks=True))
    profiles = {
        "aws": {
            "my_profile": {
                "aws_access_key_id": "my_key",
                "aws_secret_access_key": "my_secret_key",
            }
        }
    }

    ctx = NamespaceDict(test=False)

    awscli_config = {"profiles": {"default": {"region": "moon-darkside-1"}}}
    with mock.patch("botocore.configloader.load_config", return_value=awscli_config):
        # Collect the default profile
        ctx.acct = await hub.acct.init.gather(
            subs=["aws"], profile="my_profile", profiles=profiles
        )

        # verify that there is no region name
        assert not ctx.acct["region_name"]

        # Make a boto call, verify that a connection error is raised
        ret = await hub.exec.boto3.client.ec2.describe_vpcs(ctx)

        # The awscli config should have been used
        assert any("moon-darkside-1" in c for c in ret.comment), ret.comment


@pytest.mark.asyncio
async def test_defined_profile_with_region(hub):
    """
    Verify that when region_name is specified in the acct_file,
    that it does NOT get populated from ~/.aws/credentials; it should come from acct.
    """
    hub.OPT = dict_tools.data.NamespaceDict(idem=dict(awscli_fallbacks=False))
    profiles = {
        "aws": {
            "my_profile": {
                "aws_access_key_id": "my_key",
                "aws_secret_access_key": "my_secret_key",
                "region_name": "moon-tranquility-1",
            }
        }
    }

    ctx = NamespaceDict(test=False)

    awscli_config = {"profiles": {"default": {"region": "moon-darkside-1"}}}
    with mock.patch("botocore.configloader.load_config", return_value=awscli_config):
        # Collect the default profile
        ctx.acct = await hub.acct.init.gather(
            subs=["aws"], profile="my_profile", profiles=profiles
        )

        # Make a boto call, verify that a connection error is raised with the region from the profile
        ret = await hub.exec.boto3.client.ec2.describe_vpcs(ctx)
        assert any(
            'ValueError: Please verify ec2 is available in region moon-tranquility-1: Could not connect to the endpoint URL: "https://ec2.moon-tranquility-1.amazonaws.com/"'
            in c
            for c in ret.comment
        ), ret.comment
        # The region from the acct profile should be used
        assert any("moon-tranquility-1" in c for c in ret.comment), ret.comment


@pytest.mark.asyncio
async def test_no_profile(hub):
    """
    Verify that when no profile is specified, that awscli credentials/config gets used
    """
    hub.OPT = dict_tools.data.NamespaceDict(idem=dict(awscli_fallbacks=False))

    ctx = NamespaceDict(test=False)

    awscli_config = {
        "profiles": {
            "default": {
                "region": "moon-darkside-2",
                "aws_access_key_id": "my_key",
                "aws_secret_access_key": "my_secret_key",
            }
        }
    }
    with mock.patch("botocore.configloader.load_config", return_value=awscli_config):
        # Collect the default profile
        ctx.acct = await hub.acct.init.gather(
            subs=["aws"], profile="my_profile", profiles={}
        )

        # Make a boto call, verify that a connection error is raised with the region from the profile
        ret = await hub.exec.boto3.client.ec2.describe_vpcs(ctx)
        assert any(
            'ValueError: Please verify ec2 is available in region moon-darkside-2: Could not connect to the endpoint URL: "https://ec2.moon-darkside-2.amazonaws.com/"'
            in c
            for c in ret.comment
        ), ret.comment
        # The region from the acct profile should be used
        assert any("moon-darkside-2" in c for c in ret.comment), ret.comment
