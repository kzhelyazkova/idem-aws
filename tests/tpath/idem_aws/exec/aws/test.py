__func_alias__ = {"ctx_": "ctx"}
__contracts__ = ["soft_fail"]


def ctx_(hub, ctx):
    return {"ret": {k: v for k, v in ctx.acct.items() if v}, "result": True}
