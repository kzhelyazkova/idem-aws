State
=====

The *State* command allows you to ensure your environment is configured how you
specify. You specify your configuration in an *SLS* file, also known as a
*state* file.

Decribe Example
+++++++++++++++

First let's describe our VPCs. Your AWS account will have a default VPC as well
as any others you have already created.

.. code-block:: bash

    idem describe aws.ec2.vpc

Output:

.. code-block:: bash

    vpc-07fc9362:
      aws.ec2.vpc.present:
      - name: vpc-07fc9362
      - resource_id: vpc-07fc9362
      - instance_tenancy: default
      - cidr_block_association_set:
        - AssociationId: vpc-cidr-assoc-b2ca3fdb
          CidrBlock: 172.31.0.0/16
          CidrBlockState:
            State: associated
      - enable_dns_hostnames: false
      - enable_dns_support: true

Use Idem State
______________

You'll notice that `enable_dns_hostnames` is set to `false`. If I want to
change that value to `true` I can take the above output from the Describe
example and create a file with `.sls` extension and use that to update the
running configuration.

Copy the above text output to ~/my_vpc.sls, change `enable_dns_hostnames` to
`true` and then run the following command:

.. code-block:: bash

    idem state ~/my_vpc.sls

Output:

.. code-block:: yaml

    --------
          ID: vpc-07fa0462
    Function: aws.ec2.vpc.present
      Result: True
     Comment: ()
     Changes:
    old:
        ----------
        enable_dns_hostnames:
            False
    new:
        ----------
        enable_dns_hostnames:
            True

Here you'll notice that the `old` field shows us the previous value for
`enable_dns_hostnames` and the `new` field shows us the new value.
